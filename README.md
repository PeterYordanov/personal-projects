# Table of Contents

[[_TOC_]]

&nbsp;
# Interdisciplinary Software
### Artificial Intelligence

| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Rust     | [Perceptron Model](https://gitlab.com/PeterYordanov/perceptron-model "") | Complete | |
| Python   | [2D Multi-person Pose Estimation](https://gitlab.com/PeterYordanov/2d-multiperson-pose-estimation "") | Complete | |
| Python   | [Classical Music Generation](https://gitlab.com/PeterYordanov/classical-music-generation "") | Complete | |
| Python   | [Machine Learning & Quantum Computation Engine](https://gitlab.com/PeterYordanov/machine-learning-quantum-engine-api "") | Complete | |
| Python   | [Blur Detection](https://gitlab.com/PeterYordanov/blur-detection "") | Complete | |
| C++      | [Computer Vision Library](https://gitlab.com/PeterYordanov/computer-vision-library) | Complete | |
| C++      | [Linear Regression & K-Means Clustering](https://gitlab.com/PeterYordanov/machine-and-deep-learning-solutions) | Complete | |
| C++      | [AI Composite](https://gitlab.com/PeterYordanov/ai-composite "") | Complete | |
| C#       | [Google AutoML Extended](https://gitlab.com/PeterYordanov/google-automl-extended) | Complete | |

### Quantum Computing

| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Python | [Machine Learning and Quantum Computing Engine](https://gitlab.com/PeterYordanov/machine-learning-quantum-engine-api "") | Complete | |
| Python | [Quantum Breast Cancer Classification](https://gitlab.com/PeterYordanov/quantum-breast-cancer-classification "") | Complete | |
| Q# | [Quantum Gates and Operations](https://gitlab.com/PeterYordanov/quantum-gates-and-operations "") | Complete | |

### Security
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
||| | |

&nbsp;
# Application-level Software

### Desktop Applications
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| C++      | [Web Browser](https://gitlab.com/PeterYordanov/web-browser "") | Complete | |
| C++      | [OpenGL 3D Game Engine](https://gitlab.com/PeterYordanov/opengl-3d-game-engine "") | WIP | |
| C++      | [Power Utility Tool](https://gitlab.com/PeterYordanov/power-utility-tool "") | WIP | |
| C++      | [Documentations and Tools Manager](https://gitlab.com/PeterYordanov/documentations-and-tools-manager "") | WIP | |
| C#       | [Lecture Schedule Management System](https://gitlab.com/PeterYordanov/lecture-schedule-management-system "") | Complete | |
| Java     | [LWJGL 3D Game Engine](https://gitlab.com/PeterYordanov/lwjgl-3d-game-engine "") | Complete | |

### Web Applications, Web APIs
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Python   | [Machine Learning and Quantum Computation Engine API](https://gitlab.com/PeterYordanov/machine-learning-quantum-engine-api "") | Complete | |
| C#       | [TV Daily Critic](https://gitlab.com/PeterYordanov/tv-daily-critic-website "") | Complete | |
| C#       | [Car Rental](https://gitlab.com/PeterYordanov/car-rental "") | Complete | |
| C#       | [Task Management System](https://gitlab.com/PeterYordanov/task-management-system "") | Complete | |
| PHP      | [Movie Management System](https://gitlab.com/PeterYordanov/login-form-assignment "") | Complete | |
| JavaScript | [Trello Clone](https://gitlab.com/PeterYordanov/trello-clone) | Complete | |

### Mobile Applications
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Java     | [Note Taking Application](https://gitlab.com/PeterYordanov/note-taking-mobile-application "") | Complete | |

### Console Applications
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Rust     | [DNS Server](https://gitlab.com/PeterYordanov/dns-server) | Complete | |
| Rust     | [Web Crawler](https://gitlab.com/PeterYordanov/web-crawler) | Complete | |
| C#       | [Selenium YouTube ad Removing Scheduler](https://gitlab.com/PeterYordanov/selenium-youtube-ad-removing-scheduler) | Complete | |

### Libraries & Packages
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| C++      | [Shared Library](https://gitlab.com/PeterYordanov/shared-library-cpp "") | Complete | |
| C++      | [Utility Library](https://gitlab.com/PeterYordanov/cpp-utility-library "") | Complete | |
| C#       | [Automation Library](https://gitlab.com/PeterYordanov/automation-library "") | Complete | |
| C#       | [Selenium Extension Library](https://gitlab.com/PeterYordanov/selenium-extension-library "") | Complete | |

&nbsp;
# System-level Software
### Kernels, Drivers & Filesystems
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| C        | [MinimOS Kernel](https://gitlab.com/PeterYordanov/minimos-kernel "") | Complete | |
| C++      | [Cpp Kernel](https://gitlab.com/PeterYordanov/cpp-kernel "") | Complete | |
| C        | [Filesystem in Userspace FUSE](https://gitlab.com/PeterYordanov/filesystem-in-userspace-fuse "") | Complete | |
| C        | [Windows Device Driver](https://gitlab.com/PeterYordanov/windows-device-driver "") | Complete | |
| C        | [Linux USB Device Driver](https://gitlab.com/PeterYordanov/linux-usb-device-driver "") | Complete | |
| C        | [Linux Device Driver](https://gitlab.com/PeterYordanov/linux-device-driver "") | Complete | |

### Simulators, Emulators & Virtual Machines
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| Rust     | [Stack-Based Virtual Machine & Assembler](https://gitlab.com/PeterYordanov/stack-based-virtual-machine-and-assembler) | Complete | |
| C++      | [Chip-8 Emulator](https://gitlab.com/PeterYordanov/chip8-emulator "") | Complete | |

### Compilers
| Language | Project | Status | Documented README |
| ---      | ---     | ---    | ---               |
| C        | [Ivory Compiler](https://gitlab.com/PeterYordanov/ivory-compiler "") | Complete | |
| C++      | [Jade Compiler](https://gitlab.com/PeterYordanov/jade-compiler "") | Complete | |

### DevOps
| Language | Status | Documented README |
| ---      | ---    | ---               |
| [Infrastructure 1]( "") | Complete | |
| [Infrastructure 2]( "") | Complete | |
| [Linux Reinstall Setup 3](https://gitlab.com/PeterYordanov/linux-reinstall-setup-3 "") | Complete | |
| [Windows Reinstall Setup 3](https://gitlab.com/PeterYordanov/windows-reinstall-setup-3 "") | Complete | |
| [Vagrant](https://gitlab.com/PeterYordanov/vagrant "") | Complete | |
| [Nagios](https://gitlab.com/PeterYordanov/nagios "") | Complete | |
| [Jenkins](https://gitlab.com/PeterYordanov/jenkins "") | Complete | |
| [Terraform](https://gitlab.com/PeterYordanov/terraform "") | Complete | |
| [Nomad](https://gitlab.com/PeterYordanov/nomad "") | Complete | |
| [Puppet](https://gitlab.com/PeterYordanov/puppet "") | Complete | |
| [Ansible](https://gitlab.com/PeterYordanov/ansible "") | Complete | |
| [Kubernetes](https://gitlab.com/PeterYordanov/kubernetes "") | Complete | |
